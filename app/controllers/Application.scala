package controllers

import models.CoefficientComponentImpl
import play.api.mvc._

class Application extends Controller with CoefficientComponentImpl {

  def getResult(v1: Int) = Action {

    coefficientComponentModel.getResult(v1) match {
      case Left(err) =>
        BadRequest(<message status="failure">err:{err}</message>)
      case Right(result) =>
        Ok(<message status="OK">result:{result}</message>)
    }
  }

  def calculate = Action { request =>

    println(request.body)
    request.body.asXml.map { xml =>
      val calculateResult = for {
        v2 <- (xml \\ "v2" headOption).map(_.text.toInt)
        v3 <- (xml \\ "v3" headOption).map(_.text.toInt)
        v4 <- (xml \\ "v4" headOption).map(_.text.toInt)
      } yield {
          coefficientComponentModel.calculate(v2, v3, v4) match {
            case Left(err) =>
              BadRequest(<message status="failure">err:{err}</message>)
            case Right(result) =>
              Ok(<message status="OK">result:{result}</message>)
          }
        }

      calculateResult match {
        case Some(result) => result
        case _ => BadRequest("Missing parameter [name]")
      }
    }.getOrElse(BadRequest(<message status="failure">err: Expecting xml data</message>))
  }

}
