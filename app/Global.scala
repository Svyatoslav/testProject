import play.api.mvc.Results._
import play.api.mvc.{RequestHeader, Result}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

/**
 * Created by Svyatoslav Lukashkevych on 23.10.15.
 */
object Global extends play.api.GlobalSettings {

  override def onError(request: RequestHeader, ex: Throwable): Future[Result] = {
    Future(BadRequest(
      <message status="failure">err:
        {ex.getMessage}
      </message>))
  }

  override def onBadRequest(request: RequestHeader, error: String): Future[Result] = {
    Future(BadRequest(
      <message status="failure">err:
        {error}
      </message>))
  }
}
