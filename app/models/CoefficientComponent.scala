package models

import java.io._

import com.opencsv.CSVReader

import scala.collection.JavaConversions._

/**
 * Created by Svyatoslav Lukashkevych on 22.10.15.
 */
trait CoefficientComponent {
  def coefficientComponentModel: CoefficientComponentModel

  trait CoefficientComponentModel {

    def getResult(v1: Int): Either[String, Int]

    def calculate(v2: Int, v3: Int, v4: Int): Either[String, Boolean]

  }

}

trait CoefficientComponentImpl extends CoefficientComponent {

  lazy val coefficientComponentModel = new CoefficientComponentModelImpl

  class CoefficientComponentModelImpl extends CoefficientComponentModel {

    private val f1 = new File("f1.csv")
    private val f2 = new File("f2.csv")

    if (!f1.exists()) f1.createNewFile()
    if (!f2.exists()) f2.createNewFile()

    override def getResult(v1: Int): Either[String, Int] = withExceptionHandler[Int] {
      synchronized {
        val reader = new CSVReader(new FileReader(f2), ',', '\'', v1)
        val number = reader.readNext().head.toInt
        reader.close()
        if (number > 10) Right(number - 10)
        else Right(number)
      }
    }

    override def calculate(v2: Int, v3: Int, v4: Int): Either[String, Boolean] = withExceptionHandler[Boolean] {

      for{
        f1Values <- readValues(f1).right
        f2Values <- readValues(f2).right
      } yield {

        val statementResult = f1Values(v3) + v2 < 10
        f2Values(v4) =
          f1Values(v3) + v2 +
          (if(statementResult) 10 else 0)

        synchronized{
          val writer = new PrintWriter(f2)
          f2Values.foreach(number => writer.write(number + "\n"))
          writer.close()
        }
        statementResult
      }
    }

    private def readValues(file: File): Either[String, Array[Int]] = withExceptionHandler[Array[Int]] {
      synchronized {
        val reader = new CSVReader(new FileReader(file))
        val values = reader
          .readAll()
          .map[Int, Array[Int]] {
            line =>
              line.head.toInt
        }(collection.breakOut)
        reader.close()
        Right(values)
      }
    }

    private def withExceptionHandler[T](block: => Either[String, T]) =
      try block catch {
        case ex: IOException => Left("IO exception")
        case ex: NoSuchElementException => Left("no such element exception")
        case ex: NumberFormatException => Left("number format exception")
        case ex: NullPointerException => Left("null pointer exception")
        case ex: IndexOutOfBoundsException => Left("no element with such index")
        case ex => Left("Internal server error")
      }

  }

}
