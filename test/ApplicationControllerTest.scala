package controllers


import models.CoefficientComponentImpl
import org.mockito.Mockito._
import org.specs2.mock.Mockito
import play.api.mvc.Result
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}

import scala.concurrent.Future

/**
 * Created by Svyatoslav Lukashkevych on 23.10.15.
 */
class ApplicationControllerTest extends PlaySpecification with Mockito {

  object Test extends Application
  with CoefficientComponentImpl {
    override lazy val coefficientComponentModel = mock[CoefficientComponentModelImpl]
  }

  "Application" should {
    "getResult" in new WithApplication() {

      when(Test.coefficientComponentModel.getResult(1)).thenReturn(Right(1))

      val requestResult: Future[Result] = Test.getResult(1).apply(FakeRequest())

      status(requestResult) must equalTo(OK)
      contentType(requestResult) must beSome("application/xml")
    }

    "calculate" in new WithApplication() {

      when(Test.coefficientComponentModel.calculate(1, 2, 3)).thenReturn(Right(true))

      val values = <v2>1</v2> <v3>2</v3> <v4>3</v4>
      val requestResult = Test.calculate.apply(FakeRequest().withXmlBody(values))
    }
  }

}
