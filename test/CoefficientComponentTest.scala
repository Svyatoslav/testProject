package models

import java.io.{File, PrintWriter}

import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import play.api.test.PlaySpecification

/**
 * Created by Svyatoslav Lukashkevych on 22.10.15.
 */
@RunWith(classOf[JUnitRunner])
class CoefficientComponentTest extends PlaySpecification {

  object Test extends CoefficientComponentImpl

  "CoefficientComponent" should {
    "getResponse" in {

      val f2Path = "f2.csv"
      val f2 = new File(f2Path)
      f2.createNewFile()

      val writer = new PrintWriter(f2)
      val values = for (i <- 7 to 15) yield i
      values.foreach(number => writer.write(number + "\n"))
      writer.close()

      Test.coefficientComponentModel.getResult(1) mustEqual Right(values(1))
      Test.coefficientComponentModel.getResult(5) mustEqual Right(values(5) - 10)

      f2.delete()
    }

    "calculate" in {

      val f1Path = "f1.csv"
      val f1 = new File(f1Path)
      f1.createNewFile()

      val f2Path = "f2.csv"
      val f2 = new File(f2Path)
      f2.createNewFile()

      val writer1 = new PrintWriter(f1)
      val writer2 = new PrintWriter(f2)

      val values = for (i <- 1 to 5) yield i
      values.foreach(number => writer1.write(number + "\n"))
      values.foreach(number => writer2.write(number + "\n"))
      writer1.close()
      writer2.close()

      val v2 = 2
      val v3 = 3
      val v4 = 4

      Test.coefficientComponentModel.calculate(v2, v3, v4) match {
        case Left(err) => err mustEqual Right(true)
        case Right(result) =>

          Test.coefficientComponentModel.getResult(v4) mustEqual Right(
            if (result) values(v3) + v2
            else values(v3) + v2 - 10
          )
      }

      f1.delete()
      f2.delete()

    }
  }

}
